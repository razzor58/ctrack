package com.example.camera.tools

import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.OnLifecycleEvent
import java.util.concurrent.Executor

class EventsDispatcher<ListenerType>(private val mExecutor: Executor) {
    private var mEventsListener: ListenerType? = null

    internal val mBlocks = mutableListOf<ListenerType.() -> Unit>()

    fun bind(lifecycleOwner: LifecycleOwner, listener: ListenerType) {
        val observer = object : LifecycleObserver {

            @OnLifecycleEvent(Lifecycle.Event.ON_RESUME)
            fun connectListener(source: LifecycleOwner) {
                mEventsListener = listener
                mExecutor.execute {
                    mBlocks.forEach {
                        it(listener)
                    }
                    mBlocks.clear()
                }
            }

            @OnLifecycleEvent(Lifecycle.Event.ON_PAUSE)
            fun disconnectListener(source: LifecycleOwner) {
                mEventsListener = null
            }

            @OnLifecycleEvent(Lifecycle.Event.ON_DESTROY)
            fun onDestroyed(source: LifecycleOwner) {
                source.lifecycle.removeObserver(this)
            }
        }
        lifecycleOwner.lifecycle.addObserver(observer)
    }

    fun dispatchEvent(block: ListenerType.() -> Unit) {
        val eListener = mEventsListener
        if (eListener != null) {
            mExecutor.execute { block(eListener) }
        } else {
            mExecutor.execute { mBlocks.add(block) }
        }
    }

}
