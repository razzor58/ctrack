package com.example.camera

import android.os.Handler
import android.os.Looper
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.camera.feature.auth.AuthViewModel
import com.example.camera.feature.dashboard.DashboardViewModel
import com.example.camera.retrofit.RetrofitService
import com.example.camera.tools.EventsDispatcher
import java.util.concurrent.Executor

object ViewModelFactory : ViewModelProvider.NewInstanceFactory() {


    private val mainExecutor = Executor {
        val handler = Handler(Looper.getMainLooper())
        handler.post(it)
    }

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        @Suppress("UNCHECKED_CAST")
        return when {
            modelClass.isAssignableFrom(AuthViewModel::class.java) -> AuthViewModel(RetrofitService.cTrackApi, EventsDispatcher(mainExecutor)) as T
            modelClass.isAssignableFrom(DashboardViewModel::class.java) -> DashboardViewModel(RetrofitService.cTrackApi, EventsDispatcher(mainExecutor)) as T
            else -> throw IllegalArgumentException("Unknown ViewModel class: " + modelClass.name)
        }
    }

}
