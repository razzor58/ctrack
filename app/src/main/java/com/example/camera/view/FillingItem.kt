package com.example.camera.view

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.widget.FrameLayout
import androidx.annotation.DimenRes
import androidx.recyclerview.widget.RecyclerView
import com.xwray.groupie.kotlinandroidextensions.ViewHolder

/**
 * Item that fill all available free space in parent recyclerView.
 */
abstract class FillingItem(@DimenRes val minWidthResId: Int = 0,
                           @DimenRes val minHeightResId: Int = 0,
                           @DimenRes val busyWidthResId: Int = 0,
                           @DimenRes val busyHeightResId: Int = 0,
                           val calculateChildHolders: Boolean = false
) : BaseItem() {

    override fun createViewHolder(itemView: View): ViewHolder {
        val context = itemView.context
        val layoutInflater = LayoutInflater.from(context)
        val view = layoutInflater.inflate(layout, null, false)

        val fillingFrameLayout = RecyclerViewFillingFrameLayout(context)
        val layoutParams = FrameLayout.LayoutParams(FrameLayout.LayoutParams.MATCH_PARENT, FrameLayout.LayoutParams.MATCH_PARENT)
        fillingFrameLayout.addView(view, layoutParams)

        return super.createViewHolder(fillingFrameLayout)
    }

    private inner class RecyclerViewFillingFrameLayout(context: Context) : FrameLayout(context) {

        override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
            if (parent is RecyclerView) {
                val recyclerView = parent as RecyclerView
                val context = recyclerView.context
                val layoutManager = recyclerView.layoutManager

                val parentWidth = layoutManager?.let {
                    it.width - it.paddingLeft - it.paddingRight
                } ?: 0
                val parentHeight = layoutManager?.let {
                    it.height - it.paddingTop - it.paddingBottom
                } ?: 0

                var busyHeight = if (busyHeightResId != 0) {
                    context.resources.getDimensionPixelSize(busyHeightResId)
                } else {
                    0
                }
                var busyWidth = if (busyWidthResId != 0) {
                    context.resources.getDimensionPixelSize(busyWidthResId)
                } else {
                    0
                }

                if (calculateChildHolders) {
                    val childCount = recyclerView.childCount

                    for (i in 0 until childCount) {
                        val child = recyclerView.getChildAt(i)
                        if (child == this) continue

                        var marginWidth = 0
                        var marginHeight = 0

                        val lp = child.layoutParams as? MarginLayoutParams
                        lp?.let {
                            marginWidth = it.leftMargin + it.rightMargin
                            marginHeight = it.topMargin + it.bottomMargin
                        }

                        if (widthMeasureSpec == 0) {
                            busyWidth += child.measuredWidth + marginWidth
                        }
                        if (heightMeasureSpec == 0) {
                            busyHeight += child.measuredHeight + marginHeight
                        }
                    }
                }

                val minWidth = if (minWidthResId != 0) {
                    context.resources.getDimensionPixelSize(minWidthResId)
                } else {
                    0
                }

                val minHeight = if (minHeightResId != 0) {
                    context.resources.getDimensionPixelSize(minHeightResId)
                } else {
                    0
                }

                val width = Math.max(parentWidth - busyWidth, minWidth)
                val height = Math.max(parentHeight - busyHeight, minHeight)

                val widthSpec = MeasureSpec.makeMeasureSpec(if (width > 0) width else 0, MeasureSpec.EXACTLY)
                val heightSpec = MeasureSpec.makeMeasureSpec(if (height > 0) height else 0, MeasureSpec.EXACTLY)

                super.onMeasure(widthSpec, heightSpec)
            } else {
                super.onMeasure(widthMeasureSpec, heightMeasureSpec)
            }
        }
    }
}
