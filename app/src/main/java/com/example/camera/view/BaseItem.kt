package com.example.camera.view

import com.xwray.groupie.kotlinandroidextensions.Item

abstract class BaseItem : Item() {
    override fun getId(): Long = this::class.java.name.hashCode().toLong()
}
