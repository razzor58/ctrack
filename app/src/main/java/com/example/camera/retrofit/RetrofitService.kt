package com.example.camera.retrofit

import com.example.camera.BuildConfig
import io.reactivex.schedulers.Schedulers
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

object RetrofitService {

    val API_URL = "http://transport-nsk.ru/ctrack/"

    lateinit var okHttpClient: OkHttpClient
    lateinit var retrofitV1: Retrofit
    lateinit var cTrackApi: CTrackApi

    init {
        setClient(API_URL)
    }

    private fun setClient(baseUrl: String) {
        val okHttpClientBuilder = OkHttpClient.Builder()
        val httpLoggingInterceptor = HttpLoggingInterceptor()
        httpLoggingInterceptor.level = HttpLoggingInterceptor.Level.BODY
        okHttpClient = if (BuildConfig.DEBUG) {
            okHttpClientBuilder
                .addInterceptor(httpLoggingInterceptor)
                .build()
        } else {
            okHttpClientBuilder.build()
        }
        retrofitV1 = Retrofit.Builder()
            .addCallAdapterFactory(RxJava2CallAdapterFactory.createWithScheduler(Schedulers.io()))
            .addConverterFactory(GsonConverterFactory.create())
            .baseUrl(baseUrl)
            .client(okHttpClient)
            .build()
        cTrackApi = createRetrofit(retrofitV1, CTrackApi::class.java)
    }

    private fun <T> createRetrofit(retrofit: Retrofit, service: Class<T>): T {
        return retrofit.create(service)
    }

}
