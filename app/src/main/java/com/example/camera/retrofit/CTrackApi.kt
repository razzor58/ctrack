package com.example.camera.retrofit

import com.example.camera.feature.dashboard.BaseResponse
import com.example.camera.feature.dashboard.OrdersData
import io.reactivex.Single
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.Response
import retrofit2.http.*

interface CTrackApi {

    @GET("auth")
    fun testAuth(@Query("driver_code") driverCode: String,
                 @Query("token") token: String = "1ee87410-730b-4a6e-ab05-19cf01fdcdf9"): Single<Response<Void>>

    @GET("load_data")
    fun loadOrders(): Single<BaseResponse<OrdersData>>

    @Multipart
    @POST("upload_doc")
    fun uploadImage(@Query("order_id") driverCode: String, @Part file: MultipartBody.Part, @Part("name") requestBody: RequestBody): Single<Response<Void>>
}
