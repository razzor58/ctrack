package com.example.camera.feature.auth

interface AuthEventsListener {
    fun authSuccess()
    fun showToastError(errorMessage: String)
}
