package com.example.camera.feature.dashboard

import com.google.gson.annotations.SerializedName

data class BaseResponse<T>(
    @SerializedName("message")
    val message: String,
    @SerializedName("data")
    val data: T
)

data class OrdersData(
    @SerializedName("message")
    val message: String,
    @SerializedName("data")
    val data: List<OrderData>
)

data class OrderData(
    @SerializedName("model")
    val model: String,
    @SerializedName("pk")
    val pk: Int,
    @SerializedName("fields")
    val fields: OrderDataFields
)

data class OrderDataFields(
    @SerializedName("Number")
    val number: String,
    @SerializedName("Address")
    val address: String,
    @SerializedName("ExportDate")
    val exportDate: String,
    @SerializedName("ExportTime")
    val exportTime: String,
    @SerializedName("ContainerNum")
    val containerNum: String,
    @SerializedName("Comment")
    val comment: String,
    @SerializedName("attachments")
    val attachments: List<String>
)
