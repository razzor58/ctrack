package com.example.camera.feature.dashboard

import android.graphics.Bitmap
import androidx.lifecycle.ViewModel
import com.example.camera.retrofit.CTrackApi
import com.example.camera.rxjava.addTo
import com.example.camera.tools.EventsDispatcher
import io.reactivex.disposables.CompositeDisposable
import okhttp3.MediaType
import okhttp3.MultipartBody
import timber.log.Timber
import okhttp3.RequestBody
import java.io.ByteArrayOutputStream
import java.io.File
import java.io.FileOutputStream


class DashboardViewModel(
    private val cTrackApi: CTrackApi,
    val eventsDispatcher: EventsDispatcher<DashboardEventsListener>
) : ViewModel() {

    private val mCompositeDisposable: CompositeDisposable = CompositeDisposable()

    fun loadOrders() {
        cTrackApi.loadOrders()
            .subscribe({
                Timber.i(it.toString())
                eventsDispatcher.dispatchEvent { onOrderDataLoad(it.data.data) }
            }, {
                Timber.e(it)
                eventsDispatcher.dispatchEvent { showToastError(it.localizedMessage) }
            })
            .addTo(mCompositeDisposable)
    }

    fun uploadBitmap(cacheDir: File, orderId: Int, bitmap: Bitmap) {
        Timber.i("uploadBitmap orderId: $orderId bitmapByteCount: ${bitmap.byteCount}")

        // Create a file object using file path
        val file = File(cacheDir, "upload-image-$orderId")
        file.createNewFile()

        // Convert bitmap to byte array
        val outputStream = ByteArrayOutputStream()
        bitmap.compress(Bitmap.CompressFormat.JPEG, 0, outputStream)
        val bitmapdata = outputStream.toByteArray()

        // write the bytes in file
        val fileOutputStream = FileOutputStream(file)
        fileOutputStream.write(bitmapdata)
        fileOutputStream.flush()
        fileOutputStream.close()

        // Create a request body with file and image media type
        val fileReqBody = RequestBody.create(MediaType.parse("image/*"), file)
        // Create MultipartBody.Part using file request-body,file name and part name
        val part = MultipartBody.Part.createFormData("upload", file.name, fileReqBody)
        //Create request body with text description and text media type
        val description = RequestBody.create(MediaType.parse("text/plain"), "image-type")

        val uploadDisposable = cTrackApi.uploadImage(orderId.toString(), part, description)
            .doFinally {
                file.delete()
            }
            .subscribe({
                Timber.i(it.toString())
                eventsDispatcher.dispatchEvent { showToastError("order image: $orderId successfully uploaded") }
            }, {
                Timber.e(it)
                eventsDispatcher.dispatchEvent { showToastError(it.localizedMessage) }
            })
    }

    override fun onCleared() {
        mCompositeDisposable.clear()
        super.onCleared()
    }

}
