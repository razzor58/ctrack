package com.example.camera.feature.dashboard.item

import android.widget.Button
import android.widget.TextView
import com.example.camera.R
import com.example.camera.feature.dashboard.OrderData
import com.example.camera.view.BaseItem
import com.xwray.groupie.kotlinandroidextensions.ViewHolder

class OrderItem(val order: OrderData, val onUploadClick: () -> Unit = {}) : BaseItem() {

    override fun bind(viewHolder: ViewHolder, position: Int) {
        val numberValue = viewHolder.itemView.findViewById<TextView>(R.id.number_value)
        val addressValue = viewHolder.itemView.findViewById<TextView>(R.id.address_value)
        val exportDateValue = viewHolder.itemView.findViewById<TextView>(R.id.export_date_value)
        val exportTimeValue = viewHolder.itemView.findViewById<TextView>(R.id.export_time_value)
        val containerNumValue = viewHolder.itemView.findViewById<TextView>(R.id.container_num_value)
        val commentValue = viewHolder.itemView.findViewById<TextView>(R.id.comment_value)
        val attachmentsValue = viewHolder.itemView.findViewById<TextView>(R.id.attachments_value)
        val uploadButton = viewHolder.itemView.findViewById<Button>(R.id.upload_button)
        numberValue.text = order.fields.number
        addressValue.text = order.fields.address
        exportDateValue.text = order.fields.exportDate
        exportTimeValue.text = order.fields.exportTime
        containerNumValue.text = order.fields.containerNum
        commentValue.text = order.fields.comment
        attachmentsValue.text = order.fields.attachments.reduce { acc, s -> " $s " }
        uploadButton.setOnClickListener {
            onUploadClick()
        }
    }

    override fun getLayout(): Int = R.layout.item_order_layout

}
