package com.example.camera.feature.auth

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.navigation.Navigation
import com.example.camera.R
import com.example.camera.ViewModelFactory
import com.example.camera.databinding.FragmentAuthBinding

class AuthFragment : Fragment(), AuthEventsListener {

    private lateinit var mViewModel: AuthViewModel
    private lateinit var mBinding: FragmentAuthBinding

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {

        mViewModel = ViewModelFactory.create(AuthViewModel::class.java)
        mBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_auth, container, false)

        mBinding.lifecycleOwner = this
        mViewModel.eventsDispatcher.bind(this, this)

        setupView()
        return mBinding.root
    }

    private fun setupView() {
        mBinding.loginButton.apply {
            setOnClickListener {
                mViewModel.auth(mBinding.login.text.toString())
            }
        }
    }

    override fun authSuccess() {
        Navigation.findNavController(requireView()).navigate(R.id.action_auth_to_dashboard)
    }

    override fun showToastError(errorMessage: String) {
        Toast.makeText(requireContext(), errorMessage, Toast.LENGTH_LONG).show()
    }

}
