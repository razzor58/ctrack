package com.example.camera.feature.auth

import androidx.lifecycle.ViewModel
import com.example.camera.retrofit.CTrackApi
import com.example.camera.rxjava.addTo
import com.example.camera.tools.EventsDispatcher
import io.reactivex.disposables.CompositeDisposable
import timber.log.Timber

class AuthViewModel(
    private val cTrackApi: CTrackApi,
    val eventsDispatcher: EventsDispatcher<AuthEventsListener>
) : ViewModel() {

    private val mCompositeDisposable: CompositeDisposable = CompositeDisposable()

    fun auth(driverCode: String) {
        cTrackApi.testAuth(driverCode)
            .subscribe({
                Timber.i("auth ok")
                eventsDispatcher.dispatchEvent { authSuccess() }
            }, {
                Timber.e(it)
                eventsDispatcher.dispatchEvent { showToastError(it.localizedMessage) }
            })
            .addTo(mCompositeDisposable)
    }

    override fun onCleared() {
        mCompositeDisposable.clear()
        super.onCleared()
    }

}
