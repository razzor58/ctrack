package com.example.camera.feature.dashboard

import android.graphics.Bitmap
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.os.bundleOf
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.navigation.Navigation
import com.example.camera.R
import com.example.camera.ViewModelFactory
import com.example.camera.databinding.FragmentDashboardBinding
import com.example.camera.feature.dashboard.item.OrderItem
import com.xwray.groupie.GroupAdapter
import com.xwray.groupie.ViewHolder

class DashboardFragment: Fragment(), DashboardEventsListener {

    private lateinit var mViewModel: DashboardViewModel
    private lateinit var mBinding: FragmentDashboardBinding
    private lateinit var mAdapter: GroupAdapter<ViewHolder>

    private var mOrderIdForUpload: Int = -1
    private var mBitmapForUpload: Bitmap? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {

        mViewModel = ViewModelFactory.create(DashboardViewModel::class.java)
        mBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_dashboard, container, false)

        mBinding.lifecycleOwner = this
        mViewModel.eventsDispatcher.bind(this, this)

        setupView()
        return mBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val orderIdForUpload: Int = arguments?.getInt("orderId") ?: -1
        val bitmapForUpload: Bitmap? = arguments?.getParcelable("imageBitmap")
        if (orderIdForUpload != -1 && bitmapForUpload != null) {
            mViewModel.uploadBitmap(requireContext().cacheDir, orderIdForUpload, bitmapForUpload)
        }
    }

    override fun onStart() {
        super.onStart()
        mViewModel.loadOrders()
    }

    private fun setupView() {
        mAdapter = GroupAdapter()
        mBinding.recyclerView.adapter = mAdapter
        mBinding.swipeContainer.setOnRefreshListener {
            mViewModel.loadOrders()
        }
    }

    private fun onUploadPhotoClick(orderData: OrderData) {
        val bundle = bundleOf("orderId" to orderData.pk)
        Navigation.findNavController(requireView()).navigate(R.id.action_dashboard_to_camera, bundle)
    }

    override fun onProgress() {

    }

    override fun onOrderDataLoad(ordersData: List<OrderData>) {
        mBinding.swipeContainer.isRefreshing = false
        mAdapter.update(ordersData.map {
            OrderItem(it) {
                onUploadPhotoClick(it)
            }
        })
    }

    override fun showToastError(errorMessage: String) {
        mBinding.swipeContainer.isRefreshing = false
        Toast.makeText(requireContext(), errorMessage, Toast.LENGTH_LONG).show()
    }

}
