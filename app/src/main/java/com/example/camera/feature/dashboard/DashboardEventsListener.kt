package com.example.camera.feature.dashboard

interface DashboardEventsListener {
    fun onProgress()
    fun onOrderDataLoad(ordersData: List<OrderData>)
    fun showToastError(errorMessage: String)
}
